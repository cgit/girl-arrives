APPS =				\
	cairo-test		\
	von-wait		\
	cairo-bclock		\
	cairo-rule

CFLAGS  = -ggdb -Wall

CFLAGS  += `pkg-config gtk+-2.0 librsvg-2.0 --cflags `
LDFLAGS += `pkg-config gtk+-2.0 librsvg-2.0 --libs`

all: $(APPS)

clean:
	$(RM) $(APPS)
